
// TraI_21_t4_5_pohja.java SJ
// Pohja viikon 1 tehtÃ¤viin 4-5

/**
 * 4. Kirjoita algoritmi (Java-metodi) joka saa parametrinaan kaksi kokonaislukutaulukkoa (In-
 * teger[] A, Integer[] B) ja joka luo ja palauttaa uuden kokonaislukutaulukon jossa on kaikki ne
 * alkiot jotka lÃ¶ytyvÃ¤t jommastakummasta taulukosta (siis niiden yhdisteen). Kukin al- kio
 * (.equals() palauttaa toden) tulee kuitenkin tulostaulukkoon vain yhden kerran vaikka se
 * esiintyisi toisessa tai molemmissa taulukoissa useamman kerran. MikÃ¤ on algoritmisi
 * aikavaativuus? Voisiko sitÃ¤ parantaa?
 * 
 * 
 *
 * 5. Kertaa Olio-ohjelmointi -kurssilla kÃ¤sitelty ArrayList
 * -luokka. LisÃ¤tietoa: http://docs. oracle.com/javase/8/docs/api/java/util/ArrayList.html. Muuta
 * edellisen tehtÃ¤vÃ¤n algoritmi toimimaan taulukoiden sijaan ArrayList -kokoelmilla. MikÃ¤ on
 * algoritmisi aika- vaativuus? Voisiko sitÃ¤ parantaa?
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class TRAI_21_t4_5_htran {

  // PÃ¤Ã¤ohjelman kÃ¤yttÃ¶:
  // java TRAI_21_t4_5 [N] [N2] [S]
  // missÃ¤ N on alkioiden mÃ¤Ã¤rÃ¤, N2 on alkoiden mÃ¤Ã¤rÃ¤ toisessa taulukossa
  // ja S on satunnaislukusiemen
  public static void main(String[] args) {

    // taulukoiden koko
    int n1 = 10;
    if (args.length > 0) n1 = Integer.parseInt(args[0]);

    int n2 = n1 + 2;
    if (args.length > 1) n2 = Integer.parseInt(args[1]);

    // satunnaislukusiemen
    int siemen = 42;
    if (args.length > 2) siemen = Integer.parseInt(args[2]);

    // luodaan esimerkkitaulukot
    Integer[] T1 = new Integer[n1];
    Integer[] T2 = new Integer[n2];

    // tÃ¤ytetÃ¤Ã¤n alkioilla
    java.util.Random r = new java.util.Random(siemen);
    for (int i = 0; i < n1; i++) {
      T1[i] = r.nextInt(n1);
    }

    for (int i = 0; i < n2; i++) {
      T2[i] = r.nextInt(n2 * 2);
    }

    // tulostetaan taulukot jos alkioita ei ole paljoa
    if (n1 <= 20 && n2 <= 20) {
      System.out.println("T1: " + Arrays.toString(T1));
      System.out.println("T2: " + Arrays.toString(T2));
    }

    // kutsutaan tehtÃ¤vÃ¤Ã¤ 4
    Integer[] E4 = yhdiste4(T1, T2);

    System.out.print("TehtÃ¤vÃ¤ 4, yhdiste = ");
    if (n1 <= 20 && n2 <= 20) {
      System.out.println(Arrays.toString(E4));
    } else {
      System.out.println(E4.length + " alkioinen taulukko");
      // huom: tÃ¤mÃ¤ tulostaa taulukon koon, ei todellisten alkioiden mÃ¤Ã¤rÃ¤Ã¤!
    }

    // Muodostetaan taulukoista ArrayList:t

    ArrayList<Integer> L1 = new ArrayList<Integer>(T1.length);
    ArrayList<Integer> L2 = new ArrayList<Integer>(T2.length);
    for (Integer x : T1) L1.add(x);

    for (Integer x : T2) L2.add(x);

    // kutsutaan tehtÃ¤vÃ¤Ã¤ 5
    ArrayList<Integer> E5 = yhdiste5(L1, L2);

    System.out.print("TehtÃ¤vÃ¤ 5, yhdiste = ");
    if (n1 <= 20 && n2 <= 20) {
      System.out.println(E5);
    } else {
      System.out.println(E5.size() + " alkiota");
    }
  } // main()

  /**
   * 4. Palauttaa taulukoiden yhdisteen, siis ne alkiot jotka ovat taulukossa T1 ja/tai
   * T2. Kukin alkio tulee tulostaulukkoon vain kertaalleen.
   * Taulukot kÃ¤sitellÃ¤Ã¤n kokonaan, null alkiot ohitetaan.
   * Aikavaativuus: O(n)
   * Paranta: Array muuttaa ArrayListiin
   * @param T1 ensimmÃ¤inen taulukko
   * @param T2 toinen taulukko
   * @return yhdiste taulukkona
   */
  public static Integer[] yhdiste4(Integer[] T1, Integer[] T2) {

    // TODO: tÃ¤ydennÃ¤ tÃ¤mÃ¤ metodi

        
    int n = T1.length + T2.length; // tulostaulukon koko
    
        Integer[] tulos = new Integer[n];
       
        //Kopio T1 alkiot tuloksen taulukoon
        System.arraycopy(T1, 0, tulos, 0 , T1.length);
        //Kopio T2 alkiot tuloksen taulukoon
        System.arraycopy(T2, 0, tulos,  T1.length, T2.length);
 
       //Läjittele tulos taulukko
       Arrays.sort(tulos);
       
       //Poista samat alkiot
        if (n==0 || n==1){  
            return tulos;  
        }  
        Integer[] tempTulos = new Integer[n];  
        int j = 0;  
        for (int i=0; i<n-1; i++){             
            Integer alkio = tulos[i];
            if (!Objects.equals(alkio, tulos[i+1])){  
                tempTulos[j++] = alkio;  
            } 
            if(alkio == null){
                
            }
         }  
        tempTulos[j++] = tulos[n-1]; 
       int m = countUniqueValues(tempTulos);
       
      
        
         return tempTulos;
     }
  static Integer countUniqueValues(Integer[] c) {
    int unique = c.length;
    for (int i = 0; i < c.length; i++) {
        while (i + 1 < c.length && c[i] == c[i + 1]) {
            i++;
            unique--;
        }
    }
    return unique;
}

   // yhdiste4


  /**
   * 5. Palauttaa taulukkopohjaisten listojen yhdisteen uutena listana.
   * Aikavaativuus: O(n)
   * @param L1 ensimmÃ¤inen lista
   * @param L2 toinen lista
   * @return erotus listana
   */
  public static ArrayList<Integer> yhdiste5(ArrayList<Integer> L1, ArrayList<Integer> L2) {
    ArrayList<Integer> tulos = new ArrayList<>();
    //Lisätä L1, L2 tulos listalle
    tulos.addAll(L1);
    tulos.addAll(L2);
    
    //Muutu ArrayList LinkedHashSetiin
    Set <Integer> tulosJoukko = new LinkedHashSet<>();
    
    //Lisätä tulosJoukkonn tulos taulukkon
    tulosJoukko.addAll(tulos);
    
    //tyhjätään tulos talukkon
    tulos.clear();
    
    //lisätään takaisin tulos talukkoon tulosJoukkosta
    tulos.addAll(tulosJoukko);
    // TODO

    return tulos;
  } // yhdiste5()
} // class
